# Pasos para poner en funcionamiento

Referencias: 
- Ref 1: https://github.com/adrienverge/openfortivpn?tab=readme-ov-file
- Ref 2: https://www.youtube.com/watch?v=rj39Zxc8S3A

## Paso 0: Desisntalar fortinet client
```bash
sudo apt purge forticlient
```

Si quedan archivos en `/etc/fortinet` se recomienda borrarlos
```bash
sudo rm -rf /etc/forticlient
```

## Paso 1: Copiar los scripts

Hacer ejecutables los scripts
```bash
sudo chmod +x connect-forti-vpn disconnect-forti-vpn
```

Copiar los scripts en `/usr/local/bin`
```bash
sudo mv connect-forti-vpn disconnect-forti-vpn /usr/local/bin
```

## Paso 2: Configuración de la VPN

Instalar la herramineta `openfortivpn`
```bash
sudo apt install openfortivpn
```

Modificar el archivo `/etc/openfortivpn/config`
```bash
host = <IP> # IP de la VPN
port = <PORT> # Puerto de la VPN 
username = <USER> # usuario de dominio
# password = <PASSWORD> # password de usuario de dominio (OPCIONAL)
set-dns = 1 # true: toma lo que le envia el router ?
use-resolvconf = 0 # usa el resolvconf configurado por la app
# X509 certificate sha256 sum, trust only this one!
trusted-cert =  <HASH> # se obtiene al intentar loguearse en la primera conexión (Ref 2) 
```

## Paso 3: Uso

Conectarse a la VPN
```bash
sudo connect-forti-vpn <usuario-de-dominio>
```

Desconectarse de la VPN
```bash
sudo disconnect-forti-vpn
```
